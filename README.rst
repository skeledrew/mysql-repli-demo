Requires `poetry` (https://python-poetry.org/) for dependency management.

Clone repo and run `poetry install` to install demo and dependencies.

Run demo with `python mysql_repli_demo/__main__.py` and provide args.

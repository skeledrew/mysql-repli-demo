"""

"""


from pprint import pprint

import fire
from pymysqlreplication import BinLogStreamReader
from pymysqlreplication.row_event import (
    DeleteRowsEvent,
    UpdateRowsEvent,
    WriteRowsEvent,
)


def main(server_id, host="127.0.0.1", port=3306, user="root", passwd=""):
    """Start MySQL replication service."""
    db_settings = {
        "host": host,
        "port": port,
        "user": user,
        "passwd": passwd,
    }
    stream = BinLogStreamReader(
        server_id=server_id,
        connection_settings=db_settings,
        only_events=[DeleteRowsEvent, WriteRowsEvent, UpdateRowsEvent])

    for binlogevent in stream:
        for row in binlogevent.rows:
            if isinstance(binlogevent, (DeleteRowsEvent, UpdateRowsEvent, WriteRowsEvent)):
                e_type = binlogevent.__class__.__name__.replace("RowsEvent", "")
                pprint([e_type, row])
    return


if __name__ == "__main__":
    fire.Fire(main)
